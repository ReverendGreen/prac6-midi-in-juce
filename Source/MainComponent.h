/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback,
                        public Button::Listener,
                        public ComboBox::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();
    
    

    void resized() override;
    
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    void buttonClicked (Button* button) override;

    void handleIncomingMidiMessage (MidiInput* source,
                                    const MidiMessage& message) override;
    
    
private:
    //==============================================================================
    
    AudioDeviceManager audioDeviceManager;
    Label typeLabel, chanLabel, numLabel, velLabel;
    Slider chanSlider, numSlider, velSlider;
    ComboBox typeChooser;
    TextButton sendButton;
    
    
    
    
    // Creating a Scoped pointer <OF TYPE MidiOutput> to point at a Midi out device that I will create.
    ScopedPointer<MidiOutput> pMidiOut;
    
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
