/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{

    // This String Array stores the string names of all connected MIDI devices.
    StringArray devices(MidiInput::getDevices());

    // This for loop then iterates through the strings, printing them and setting them as enabled.
    for (int i = 0; i < devices.size(); i++)
    {
        
        std::cout << devices[i] << std::endl;
        audioDeviceManager.setMidiInputEnabled(devices[i], true);
    }

    audioDeviceManager.addMidiInputCallback(String::empty, this);
    
    
    // COMPONENT SETUP==================================
    
    chanSlider.setSliderStyle(juce::Slider::IncDecButtons);
    numSlider.setSliderStyle(juce::Slider::IncDecButtons);
    velSlider.setSliderStyle(juce::Slider::IncDecButtons);
    
    
    // EVEN THOUGH THE AUTOCOMPLETE DOESN'T TELL YOU, THE THIRD ARGUMENT IN SET RANGE ADJUSTS THE INTERVAL.s
    chanSlider.setRange(1, 16, 1);
    numSlider.setRange(0, 127, 1);
    velSlider.setRange(0, 127, 1);
    
    chanSlider.setIncDecButtonsMode(juce::Slider::incDecButtonsDraggable_Vertical);
    numSlider.setIncDecButtonsMode(juce::Slider::incDecButtonsDraggable_Vertical);
    velSlider.setIncDecButtonsMode(juce::Slider::incDecButtonsDraggable_Vertical);
 
    addAndMakeVisible(chanSlider);
    addAndMakeVisible(numSlider);
    addAndMakeVisible(velSlider);
    
    typeChooser.addItem("Note", 1);
    typeChooser.addItem("CC", 2);
    typeChooser.addItem("Prog Change", 3);
    typeChooser.setSelectedId(1);
    typeChooser.addListener(this);
    
    
    
    
    addAndMakeVisible(typeChooser);
    
    typeLabel.setText("Message Type", dontSendNotification);
    chanLabel.setText("Channel", dontSendNotification);
    numLabel.setText("Number", dontSendNotification);
    velLabel.setText("Velocity", dontSendNotification);
    
    // set the bool 'onLeft' to false to attach the label to the top.
    typeLabel.attachToComponent(&typeChooser, false);
    chanLabel.attachToComponent(&chanSlider, false);
    numLabel.attachToComponent(&numSlider, false);
    velLabel.attachToComponent(&velSlider, false);
    
    sendButton.setButtonText("Send!");
    addAndMakeVisible(sendButton);
    sendButton.addListener(this);
    
    
    
    
    
   
    // This sets my ScopedPointer (See MainComponent.h) to point at My new device that will register as a Midi device-
    // - elsewhere on my computer once the program is built.
    pMidiOut = MidiOutput::createNewDevice("Neil's MIDI program");
    
    

    
    
    setSize (500, 400);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);

}

void MainComponent::resized()
{
    typeChooser.setBounds(0, 30, getWidth()/ 5 , 30);
    chanSlider.setBounds(1 *(getWidth() / 5), 30, getWidth()/ 5 , 30);
    numSlider.setBounds(2 *(getWidth() / 5), 30, getWidth()/ 5 , 30);
    velSlider.setBounds(3 *(getWidth() / 5), 30, getWidth()/ 5 , 30);
    sendButton.setBounds(4 *(getWidth() / 5), 30, getWidth()/ 5 , 30);
    

}

void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)

{
    if (&typeChooser == comboBoxThatHasChanged)
    
    {
        if (typeChooser.getSelectedId() == 1)
            
        {
            numLabel.setText("Number", dontSendNotification);
            velLabel.setText("Velocity", dontSendNotification);
            velLabel.setVisible(true);
            velSlider.setVisible(true);
        }
        
        if (typeChooser.getSelectedId() == 2)
            
        {
            numLabel.setText("Controller ", dontSendNotification);
            velLabel.setText("  Value", dontSendNotification);
            velLabel.setVisible(true);
            velSlider.setVisible(true);
        }

        if (typeChooser.getSelectedId() == 3)
            
        {
            numLabel.setText("Program Number", dontSendNotification);
            velLabel.setVisible(false);
            velSlider.setVisible(false);
        }

    }
}

void MainComponent::buttonClicked (Button* button)

{
  
    MidiMessage message;
    if (&sendButton == button)
    
    {
        //NOTE_ON
        if (typeChooser.getSelectedId() == 1)
        
        {
            message = MidiMessage::noteOn(chanSlider.getValue(), numSlider.getValue(), (uint8)velSlider.getValue());
           

        }
        
        // CC
        else if (typeChooser.getSelectedId() == 2)
            
        {
            message = MidiMessage::controllerEvent(chanSlider.getValue(), numSlider.getValue(), velSlider.getValue());
            
        }
        
        // Program
        else if (typeChooser.getSelectedId() == 3)
            
        {
            message = MidiMessage::programChange(chanSlider.getValue(), numSlider.getValue());
            
        }
        
        String debug;
        debug << message.getDescription();
        DBG(debug);
        pMidiOut->sendMessageNow(message);

    }
}

void MainComponent:: handleIncomingMidiMessage (MidiInput* source,
                                const MidiMessage& message)

{
    

    
//    pMidiOut->sendMessageNow(message);
    
}
